# 简介
Google Guava 是一个由 Google 开发的 Java 开源函数库。前身是 Google Collections Library，提供了许多
简化工具，如缓存、连接器、过滤器、关联数组等

# 其他
1. 相关的功能测试代码在 src/test/java/org/spring/guava/app/config 目录中
2. 原先流行的 Guava 类库现在可能不再流行，甚至被包含在 JDK 中，如不可变列表，Objects 类等

# Guava 官网

https://github.com/google/guava

wiki 中，有大量示例
![img.png](doc/png/img.png)

