package org.spring.guava.app.config;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.Weigher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class GuavaConfig {
    @Bean
    public Cache<String, String> cache() {
        return CacheBuilder.newBuilder()
                // 最大内存容量为 100 条缓存，达到上限后使用 LRU 进行内存驱逐
                .maximumSize(100)
                // 3 分钟后过期
                .expireAfterWrite(3, TimeUnit.MINUTES)
                // 开启统计功能
                .recordStats()
                .build();
    }
}
