package org.spring.guava.domain.user.entity;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
public class UserEntity {
    private String userName;
    private String password;
    private Double amount;
    private Date createTime;
}
