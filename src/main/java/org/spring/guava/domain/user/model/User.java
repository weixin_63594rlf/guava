package org.spring.guava.domain.user.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class User {
    private String userName;
    private String password;
    private Double amount;
    private Date createTime;
}
